﻿#include <iostream>

void PrintNumbers(int n)
{
    int i = 0;
    if (n % 2 != 0) i = 1;
   
    while (i <= n) {
        std::cout << i << " ";
        i += 2;
    }

}
int main()
{
    int n = 7;
    std::cout << "Print even numbers:" << std::endl;
    for (int i = 0; i <= n; ++i)
    {
        if (i % 2 == 0) std::cout << i << " ";
    }
    std::cout << std::endl; // для переноса строки 

    std::cout << "Print even or odd numbers with function:" << std::endl;
    //В зависимости от параметра n (чётный или нечётный) печатает
    //соответственно чётные или нечётные числа
    PrintNumbers(11);
}


